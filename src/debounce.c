#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <limits.h>
#include <errno.h>
#include <time.h>
#include <threads.h> 
#include "config.h"

#ifndef DEBOUNCE_BUFFER_INITIAL_SIZE
#define DEBOUNCE_BUFFER_INITIAL_SIZE 65536
#endif

#ifndef DEBOUNCE_BUFFER_MAX_SIZE
#define DEBOUNCE_BUFFER_MAX_SIZE 1048576
#endif

#ifndef DEBOUNCE_BUFFER_GROWTH_MUL
#define DEBOUNCE_BUFFER_GROWTH_MUL 2
#endif

#ifndef DEBOUNCE_BUFFER_GROWTH_ADD
#define DEBOUNCE_BUFFER_GROWTH_ADD 0
#endif

struct debounce_cli_options {
    char const * program;
    unsigned long timeout;
    char input_terminator;
    char output_terminator;
    bool flush;
    bool help;
};

enum debounce_cli_parse_state {
    DEBOUNCE_CLI_PARSE_STATE_FLAG = 1,
    DEBOUNCE_CLI_PARSE_STATE_TIMEOUT = 2
};

enum debounce_cli_result_tag {
    DEBOUNCE_CLI_RESULT_OK = 1,
    DEBOUNCE_CLI_RESULT_ERROR = 2
};

struct debounce_cli_options_result {
    enum debounce_cli_result_tag tag;
    union {
        struct debounce_cli_options options;
    };
};

enum debounce_status {
    DEBOUNCE_STATUS_WAIT = 1,
    DEBOUNCE_STATUS_READY = 2,
    DEBOUNCE_STATUS_END = 3,
};

struct debounce_line {
    size_t capacity;
    size_t length;
    char * buffer;
};

struct debounce_context {
    struct debounce_cli_options const options;
    mtx_t mutex;
    cnd_t cv;
    struct debounce_line line;
    enum debounce_status status;
};





static void debounce_cli_usage(char const * program) {
    char const * usage_format = 
"\
debounce v%s - utility program for debouncing lines from stdin, prints the last line after period of no new lines\n\
\n\
usage:\n\
    %s [-t | --timeout timeout]\n\
\n\
options:\n\
    -h | --help                 show usage\n\
    -t | --timeout timeout      timeout in miliseconds (default: 1 second)\n\
    -0 | --null-terminated      items in both input and output stream are separated by a null instead of new line\n\
    --input-null-terminated     items in input stream are separated by a null instead of new line\n\
    --output-null-terminated    items in output stream are separated by a null instead of new line\n\
    --flush                     flush output for each line\n\
";

    fprintf(stderr, usage_format, VERSION, program);
}





// producer thread function
// reads characters from stdin into a buffer
//
// when new line or eof is encountered
// it copies line to shared buffer 
// and signals consumer thread 
static int debounce_producer(void * arg) {
    struct debounce_context * context = arg;

    // set up line buffer for reading from stdin
    struct debounce_line line = {
        .buffer = malloc(DEBOUNCE_BUFFER_INITIAL_SIZE),
        .length = 0,
        .capacity = DEBOUNCE_BUFFER_INITIAL_SIZE
    };

    // if malloc failed
    // inform consumer thread and exit
    if (line.buffer == NULL) {
        fprintf(stderr, "%s: malloc returned NULL\n", context->options.program);
        goto producer_return;
    }

    // main loop
    // for reading characters
    // from stdin
    while (true) {
        int const c = fgetc(stdin);

        // if eof or newline is encountered
        // then line is ready
        if (c == EOF || c == context->options.input_terminator) {
            // terminate string with nul terminator
            line.buffer[line.length] = '\0';
            line.length += 1;

            // wait until producer thread
            // signals that it is ready
            // for receiving a line
            mtx_lock(&context->mutex);
            while (context->status == DEBOUNCE_STATUS_READY) {
                cnd_wait(&context->cv, &context->mutex);
            }

            // if producer ended abruptly
            // exit this thread too
            if (context->status == DEBOUNCE_STATUS_END) {
                mtx_unlock(&context->mutex);
                goto producer_cleanup;
            }

            // dont send line if it
            // is empty and this is 
            // the end of stdin
            if (!(c == EOF && line.length == 1)) {  
                // check if shared buffer
                // has enough capacity
                // and if it doesn't 
                // try allocating enough
                if (context->line.capacity < line.length) {
                    size_t const new_capacity = line.capacity;
                    free(context->line.buffer);
                    char * new_buffer = malloc(new_capacity);

                    // malloc failed
                    // signal consumer thread
                    // and exit
                    if (new_buffer == NULL) {
                        fprintf(stderr, "%s: malloc returned NULL\n", context->options.program);
                        mtx_unlock(&context->mutex);
                        goto producer_return;
                    }

                    context->line.capacity = new_capacity;
                    context->line.buffer = new_buffer;
                }

                // copy line into the shared buffer
                memcpy(context->line.buffer, line.buffer, line.length);
                context->line.length = line.length;
                
                // and signal consumer thread
                context->status = DEBOUNCE_STATUS_READY;
                cnd_signal(&context->cv);
            }

            mtx_unlock(&context->mutex);

            // rewind buffer
            line.length = 0;

            // if stdin ended
            // inform consumer thread
            // and exit
            if (c == EOF) {
                goto producer_return;
            }
        }
        // append another character
        // to buffer
        else {
            // ensure enough capacity for
            // character and nul terminator
            if (line.length >= line.capacity - 1) {
                size_t const new_capacity = line.capacity * DEBOUNCE_BUFFER_GROWTH_MUL + DEBOUNCE_BUFFER_GROWTH_ADD;

                // 1 MiB limit is probably sane compromise
                // for simple log manipulation
                if (new_capacity > DEBOUNCE_BUFFER_MAX_SIZE) {
                    fprintf(stderr, "%s: buffer size exceeding %d bytes, bailing out\n", context->options.program, DEBOUNCE_BUFFER_MAX_SIZE);
                    goto producer_return;
                }

                char * new_buffer = realloc(line.buffer, new_capacity);

                if (new_buffer == NULL) {
                    goto producer_return;
                }

                line.capacity = new_capacity;
                line.buffer = new_buffer;
            }

            // append character
            line.buffer[line.length] = (char) c;
            line.length += 1;
        }
    }

producer_return:
    // loop until consumer thread is ready
    // so we can signal it to exit too
    mtx_lock(&context->mutex);
    while (context->status == DEBOUNCE_STATUS_READY) {
        cnd_wait(&context->cv, &context->mutex);
    }

    if (context->status != DEBOUNCE_STATUS_END) {
        context->status = DEBOUNCE_STATUS_END;
        cnd_signal(&context->cv);
    }
    mtx_unlock(&context->mutex);

producer_cleanup:
    free(line.buffer);

    return 0;
}





// consumer thread function
// waits for the producer thread to 
// send stdin line through shared buffer
static int debounce_consumer(void * arg) {
    struct debounce_context * context = arg;

    // set up line buffer for lines from shared buffer
    struct debounce_line line = {
        .buffer = malloc(DEBOUNCE_BUFFER_INITIAL_SIZE),
        .length = 0,
        .capacity = DEBOUNCE_BUFFER_INITIAL_SIZE
    };

    // if malloc failed
    // inform producer thread and exit
    if (line.buffer == NULL) {
        fprintf(stderr, "%s: malloc returned NULL\n", context->options.program);
        goto consumer_return;
    }

    // outer loop
    // awaits first line from producer thread
    // and starts inner loop with timer
    while (true) {
        // signal producer thread that
        // we are ready for receiving
        // next line
        mtx_lock(&context->mutex);
        context->status = DEBOUNCE_STATUS_WAIT;
        cnd_signal(&context->cv);

        while (context->status == DEBOUNCE_STATUS_WAIT) {
            cnd_wait(&context->cv, &context->mutex);
        }

        // if producer ended abruptly
        // exit too
        if (context->status == DEBOUNCE_STATUS_END) {
            mtx_unlock(&context->mutex);
            goto consumer_cleanup;
        }

        // ensure enough capacity
        // to copy from shared buffer
        // to local buffer
        if (context->line.length > line.capacity) {
            size_t const new_capacity = context->line.capacity;
            char * new_buffer = realloc(line.buffer, new_capacity);

            // malloc failed
            // signal producer thread
            // and exit
            if (new_buffer == NULL) {
                mtx_unlock(&context->mutex);
                goto consumer_return;
            }

            line.capacity = new_capacity;
            line.buffer = new_buffer;
        }

        // copy shared buffer to local buffer
        memcpy(line.buffer, context->line.buffer, context->line.length);
        line.length = context->line.length;
        mtx_unlock(&context->mutex);
        
        // inner loop
        // loops until timeout or
        // end of stream is encountered
        while (true) {
            // get current time
            struct timespec wait;
            timespec_get(&wait, TIME_UTC);

            // seconds = miliseconds / 10^3
            wait.tv_sec += ((time_t) context->options.timeout / 1000);
            // nanoseconds = (miliseconds - seconds) * 10^6
            wait.tv_nsec += ((time_t) (context->options.timeout % 1000) * 1000000);

            // signal producer thread
            // that we are ready
            // for receiving
            mtx_lock(&context->mutex);
            context->status = DEBOUNCE_STATUS_WAIT;
            cnd_signal(&context->cv);

            // timed wait for
            int timeout = thrd_success; 
            while (context->status == DEBOUNCE_STATUS_WAIT && timeout == thrd_success) {
                timeout = cnd_timedwait(&context->cv, &context->mutex, &wait);
            }

            // if producer thread
            // didn't send a line in timeout period
            // then print it and go back to outer loop
            if (timeout == thrd_timedout) {
                printf("%s%c", line.buffer, context->options.output_terminator);
                if (context->options.flush) { 
                    fflush(stdout);
                }

                mtx_unlock(&context->mutex);
                break;
            }
            // or if producer thread exited
            // then print what was set before
            else if (context->status == DEBOUNCE_STATUS_END) {
                printf("%s%c", line.buffer, context->options.output_terminator);
                if (context->options.flush) { 
                    fflush(stdout);
                }

                mtx_unlock(&context->mutex);
                goto consumer_cleanup;
            }
            // otherwise we continue
            // the inner loop
            // with new line
            else {
                // ensure local buffer
                // has enough capacity
                // to copy shared buffer
                if (context->line.length > line.capacity) {
                    size_t const new_capacity = context->line.capacity;
                    char * new_buffer = realloc(line.buffer, new_capacity);

                    // signal producer thread
                    // and exit
                    if (new_buffer == NULL) {
                        fprintf(stderr, "%s: malloc returned null\n", context->options.program);
                        mtx_unlock(&context->mutex);
                        goto consumer_return;
                    }


                    line.capacity = new_capacity;
                    line.buffer = new_buffer;
                }

                // copy shared buffer
                // to local buffer
                memcpy(line.buffer, context->line.buffer, context->line.length);
                line.length = context->line.length;
                mtx_unlock(&context->mutex);
            }
        }
    }

consumer_return:
    // loop until producer thread is ready
    // so we can signal it to exit too
    mtx_lock(&context->mutex);
    while (context->status == DEBOUNCE_STATUS_WAIT) {
        cnd_wait(&context->cv, &context->mutex);
    }

    if (context->status != DEBOUNCE_STATUS_END) {
        context->status = DEBOUNCE_STATUS_END;
        cnd_signal(&context->cv);
    }
    mtx_unlock(&context->mutex);

consumer_cleanup:
    free(line.buffer);

    return 0;
}

static int debounce_cli(struct debounce_cli_options const options) {
    // print help and exit on -h | --help
    if (options.help) {
        debounce_cli_usage(options.program);

        return EXIT_SUCCESS;
    }

    // set up shared thread context
    struct debounce_context context = {
        .options = options,
        .status = DEBOUNCE_STATUS_READY,
        .line = (struct debounce_line) {
            .capacity = DEBOUNCE_BUFFER_INITIAL_SIZE,
            .length = 0,
            .buffer = malloc(DEBOUNCE_BUFFER_INITIAL_SIZE)
        },
    };

    if (context.line.buffer == NULL) {
        fprintf(stderr, "%s: malloc returned null\n", options.program);
        return EXIT_FAILURE;
    }

    // initialize mt primitives
    mtx_init(&context.mutex, mtx_timed);
    cnd_init(&context.cv);

    thrd_t consumer;
    thrd_t producer;

    thrd_create(&producer, &debounce_producer, &context);
    thrd_create(&consumer, &debounce_consumer, &context);

    int rc = 0;

    // cleanup
    thrd_join(consumer, &rc);
    thrd_join(producer, &rc);

    mtx_destroy(&context.mutex);
    cnd_destroy(&context.cv);

    free(context.line.buffer);

    return EXIT_SUCCESS;
}





// state machine based argument parser
static struct debounce_cli_options_result debounce_cli_options_parse(int argc, char const *const * argv) {
    struct debounce_cli_options options = { 
        .program = argv[0],
        .timeout = 1000,
        .input_terminator = '\n',
        .output_terminator = '\n',
        .flush = false,
        .help = false
    };
    enum debounce_cli_parse_state state = DEBOUNCE_CLI_PARSE_STATE_FLAG;

    for (int i = 1; i < argc; ++i) {
        char const *const current = argv[i];

        switch (state) {
            case DEBOUNCE_CLI_PARSE_STATE_FLAG:
                (void) 0;

                bool const is_flag_h = strcmp(current, "-h") == 0;
                bool const is_flag_help = strcmp(current, "--help") == 0;

                if (is_flag_h || is_flag_help) {
                    options.help = true;
                    break;
                }

                bool const is_flag_t = strcmp(current, "-t") == 0;
                bool const is_flag_timeout = strcmp(current, "--timeout") == 0;

                if (is_flag_t || is_flag_timeout) {
                    state = DEBOUNCE_CLI_PARSE_STATE_TIMEOUT;
                    break;
                }

                bool const is_flag_0 = strcmp(current, "-0") == 0;
                bool const is_flag_null_terminated = strcmp(current, "--null-terminated") == 0;

                if (is_flag_0 || is_flag_null_terminated) {
                    options.input_terminator = '\0';
                    options.output_terminator = '\0';
                    break;
                }

                bool const is_flag_input_null_terminated = strcmp(current, "--input-null-terminated") == 0;

                if (is_flag_input_null_terminated) {
                    options.input_terminator = '\0';
                    break;
                }

                bool const is_flag_output_null_terminated = strcmp(current, "--output-null-terminated") == 0;

                if (is_flag_output_null_terminated) {
                    options.output_terminator = '\0';
                    break;
                }

                bool const is_flag_flush = strcmp(current, "--flush") == 0;

                if (is_flag_flush) {
                    options.flush = true;
                    break;
                }

                fprintf(stderr, "%s: unrecognized option \"%s\", use --help for usage\n", options.program, current);

                return (struct debounce_cli_options_result) {
                    .tag = DEBOUNCE_CLI_RESULT_ERROR
                };

            case DEBOUNCE_CLI_PARSE_STATE_TIMEOUT:
            default:
                (void) 0;

                char * end = NULL;
                unsigned long timeout = strtoul(current, &end, 10);

                if (errno == ERANGE) {
                    fprintf(stderr, "%s: timeout out of range, must be between 0 and %lu, got %s\n", options.program, ULONG_MAX, current);

                    return (struct debounce_cli_options_result) {
                        .tag = DEBOUNCE_CLI_RESULT_ERROR
                    };
                }

                if (timeout == 0 || current[0] == '-') {
                    fprintf(stderr, "%s: invalid timeout\n", options.program);

                    return (struct debounce_cli_options_result) {
                        .tag = DEBOUNCE_CLI_RESULT_ERROR
                    };
                }

                options.timeout = timeout;
                state = DEBOUNCE_CLI_PARSE_STATE_FLAG;
                break;
        }
    }

    if (state == DEBOUNCE_CLI_PARSE_STATE_TIMEOUT) {
        fprintf(stderr, "%s: -t|--timeout option requires a value\n", options.program);

        return (struct debounce_cli_options_result) {
            .tag = DEBOUNCE_CLI_RESULT_ERROR
        };
    }

    return (struct debounce_cli_options_result) {
        .tag = DEBOUNCE_CLI_RESULT_OK,
        .options = options
    };
}





extern int main(int argc, char const *const * argv) {
    struct debounce_cli_options_result const parsed = debounce_cli_options_parse(argc, argv);

    switch (parsed.tag) {
        case DEBOUNCE_CLI_RESULT_OK:
            return debounce_cli(parsed.options);

        case DEBOUNCE_CLI_RESULT_ERROR:
        default:
            return EXIT_FAILURE;
    }
}


# debounce

`debounce` is a small utility program for debouncing lines from stdin.

## Installation

Manual:

```bash
autoreconf -i
./configure
make
make install
```

Arch Linux (AUR):

```bash
paru -S debounce
```

Gentoo Linux:

Enable my overlay, see [codeberg.org/coralpink/gentoo](https://codeberg.org/coralpink/gentoo).

```bash
emerge --ask app-misc/debounce::coralpink
```

Void Linux (xbps-src):

See [codeberg.org/coralpink/void](https://codeberg.org/coralpink/void).

## Usage

Read events from the X server. Prints the last line after 1 second of no input.

```sh
xev -root -1 | debounce
```

Read access log from nginx. Prints the last log line after 1 second of no new logs.

```sh
tail -n 0 -f /var/log/nginx/access_log | debounce
```

Timeout of 3 seconds: prints `baz` after 6 seconds and `qux` after 9 seconds.

```sh
{
    echo foo; sleep 2 # new line before 3 second timeout, skip
    echo bar; sleep 1 # new line before 3 second timeout, skip
    echo baz; sleep 5 # no new line in 3 second timeout, debounce prints 'baz'
    echo qux; sleep 1 # execution ends and stdin closes, debounce prints 'qux'
} | debounce -t 3000 # 3000 ms = 3 s
```
